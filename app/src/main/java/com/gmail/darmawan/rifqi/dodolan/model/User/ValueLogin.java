package com.gmail.darmawan.rifqi.dodolan.model.User;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yolo on 28/06/18.
 */

public class ResultLogin {
    @SerializedName("username")
    String username;
    @SerializedName("password")
    String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

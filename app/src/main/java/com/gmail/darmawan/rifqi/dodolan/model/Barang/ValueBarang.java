package com.gmail.darmawan.rifqi.dodolan.model.Barang;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yolo on 20/06/18.
 */

public class ResultBarang {
    @SerializedName("nama_barang")
    private String nama_barang;
    @SerializedName("kategori")
    private String kategori;
    @SerializedName("ket_barang")
    private String ket_barang;
    @SerializedName("harga")
    private String harga;
    @SerializedName("foto")
    private String foto;

    public ResultBarang(String nama_barang, String kategori, String ket_barang, String harga, String foto) {
        this.nama_barang = nama_barang;
        this.kategori = kategori;
        this.ket_barang = ket_barang;
        this.harga = harga;
        this.foto = foto;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getKet_barang() {
        return ket_barang;
    }

    public void setKet_barang(String ket_barang) {
        this.ket_barang = ket_barang;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}

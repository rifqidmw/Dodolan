package com.gmail.darmawan.rifqi.dodolan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gmail.darmawan.rifqi.dodolan.api.BaseApiService;
import com.gmail.darmawan.rifqi.dodolan.api.UrlApi;
import com.gmail.darmawan.rifqi.dodolan.model.Barang.ResultBarang;
import com.gmail.darmawan.rifqi.dodolan.model.User.ResponseRegister;
import com.gmail.darmawan.rifqi.dodolan.model.User.ResultRegister;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignupActivity extends AppCompatActivity {

    TextView login_here;
    Button btn_signup;
    EditText et_email, et_nama, et_password;
    TextInputLayout lay_email, lay_nama, lay_password;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        lay_email = (TextInputLayout) findViewById(R.id.lay_email);
        lay_nama = (TextInputLayout) findViewById(R.id.lay_nama);
        lay_password = (TextInputLayout) findViewById(R.id.lay_password);
        et_email = (EditText) findViewById(R.id.et_email);
        et_nama = (EditText) findViewById(R.id.et_nama);
        et_password = (EditText) findViewById(R.id.et_password);
        btn_signup = (Button) findViewById(R.id.btn_signup);
        login_here = (TextView) findViewById(R.id.btn_login_here);

        login_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog = new ProgressDialog(SignupActivity.this);
                progressDialog.setMessage("Loading Register ....");
                progressDialog.show();

                if (et_email.getText().toString().equals("") && et_nama.getText().toString().equals("") && et_password.getText().toString().equals("")){
                    progressDialog.dismiss();
                    et_email.setError("Email harus diisi !!!");
                    et_nama.setError("Nama harus diisi !!!");
                    et_password.setError("Password harus diisi !!!");
                }
                else if (et_email.getText().toString().equals("") && et_nama.getText().toString().equals("")){
                    progressDialog.dismiss();
                    et_email.setError("Email harus diisi !!!");
                    et_nama.setError("Nama harus diisi !!!");
                }
                else if (et_email.getText().toString().equals("") && et_password.getText().toString().equals("")){
                    progressDialog.dismiss();
                    et_email.setError("Email harus diisi !!!");
                    et_password.setError("Password harus diisi !!!");
                }
                else if (et_nama.getText().toString().equals("") && et_password.getText().toString().equals("")){
                    progressDialog.dismiss();
                    et_nama.setError("Nama harus diisi !!!");
                    et_password.setError("Password harus diisi !!!");
                }
                else if (et_email.getText().toString().equals("")){
                    progressDialog.dismiss();
                    et_email.setError("Email harus diisi !!!");
                }
                else if (et_nama.getText().toString().equals("")){
                    progressDialog.dismiss();
                    et_nama.setError("Nama harus diisi !!!");
                }
                else if (et_password.getText().toString().equals("")){
                    progressDialog.dismiss();
                    et_password.setError("Password harus diisi !!!");
                }
                else {
                    progressDialog.dismiss();
                    ResultRegister resultRegister = new ResultRegister();
                    resultRegister.setUsername(et_email.getText().toString());
                    resultRegister.setNama(et_nama.getText().toString());
                    resultRegister.setPassword(et_password.getText().toString());

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(UrlApi.URL_DODOLAN)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    BaseApiService service = retrofit.create(BaseApiService.class);
                    Call<ResponseRegister> call = service.register(resultRegister);

                    call.enqueue(new Callback<ResponseRegister>() {
                        @Override
                        public void onResponse(Call<ResponseRegister> call, Response<ResponseRegister> response) {
                            if (response.isSuccessful()){
                                progressDialog.dismiss();
                                if (response.body().getStatus().equals("200")){
                                    String message = response.body().getMessage();
                                    Intent intent = new Intent (SignupActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(SignupActivity.this, message, Toast.LENGTH_SHORT).show();
                                } else {
                                    String message = response.body().getMessage();
                                    Toast.makeText(SignupActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(SignupActivity.this, "Gagal terhubung ke server !!!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseRegister> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(SignupActivity.this, "Tidak dapat terhubung ke internet !!!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        });
    }
}

package com.gmail.darmawan.rifqi.dodolan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gmail.darmawan.rifqi.dodolan.R;
import com.gmail.darmawan.rifqi.dodolan.model.Barang.ResultBarang;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by yolo on 20/06/18.
 */

public class MainBarangAdapter extends RecyclerView.Adapter<MainBarangAdapter.RecyclerViewHolders> {

    private List<ResultBarang> itemList;
    private Context context;

    public MainBarangAdapter(Context context, List<ResultBarang> itemList) {
        this.itemList = itemList;
        this.context = context;
    }


    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_barang_main, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.nama_barang.setText(itemList.get(position).getNama_barang());
        holder.harga_barang.setText(itemList.get(position).getHarga());
        final String urlFoto = "http://192.168.43.93/tugas_mobile/public/images/barang/" + itemList.get(position).getFoto();
        Picasso.with(context).load(urlFoto).into(holder.foto);
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView nama_barang, harga_barang;
        public ImageView foto;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            nama_barang = (TextView)itemView.findViewById(R.id.nama_barang);
            harga_barang = (TextView)itemView.findViewById(R.id.harga_barang);
            foto = (ImageView)itemView.findViewById(R.id.foto);
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(view.getContext(), "Barang = " + getPosition(), Toast.LENGTH_SHORT).show();
        }
    }
}

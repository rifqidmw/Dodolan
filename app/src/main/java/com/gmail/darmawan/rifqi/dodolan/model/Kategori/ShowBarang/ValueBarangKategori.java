package com.gmail.darmawan.rifqi.dodolan.model.Kategori;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yolo on 02/07/18.
 */

public class ValueBarangKategori {
    @SerializedName("id_kategori")
    String id_kategori;

    public String getId_kategori() {
        return id_kategori;
    }

    public void setId_kategori(String id_kategori) {
        this.id_kategori = id_kategori;
    }
}

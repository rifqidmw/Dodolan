package com.gmail.darmawan.rifqi.dodolan.api;

import com.gmail.darmawan.rifqi.dodolan.model.Barang.ResultBarang;
import com.gmail.darmawan.rifqi.dodolan.model.Ongkir.Biaya.ItemCost;
import com.gmail.darmawan.rifqi.dodolan.model.Ongkir.Kota.ItemKota;
import com.gmail.darmawan.rifqi.dodolan.model.Ongkir.Provinsi.ItemProvinsi;
import com.gmail.darmawan.rifqi.dodolan.model.User.ResponseLogin;
import com.gmail.darmawan.rifqi.dodolan.model.User.ResponseRegister;
import com.gmail.darmawan.rifqi.dodolan.model.User.ResultLogin;
import com.gmail.darmawan.rifqi.dodolan.model.User.ResultRegister;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by yolo on 12/06/18.
 */

public interface BaseApiService {

    @GET("province")
    @Headers("key:c6c41dc52d4fcb028120c688c2a76655")
    Call<ItemProvinsi> getProvince ();

    @GET("city")
    @Headers("key:c6c41dc52d4fcb028120c688c2a76655")
    Call<ItemKota> getCity (@Query("province") String province);

    @FormUrlEncoded
    @POST("cost")
    Call<ItemCost> getCost (@Field("key") String Token,
                            @Field("origin") String origin,
                            @Field("destination") String destination,
                            @Field("weight") String weight,
                            @Field("courier") String courier);

    @GET("show_barang")
    Call<List<ResultBarang>> getBarang();

    @POST("register")
    Call<ResponseRegister> register(@Body ResultRegister resultRegister);

    @POST("login")
    Call<ResponseLogin> login(@Body ResultLogin resultLogin);
}

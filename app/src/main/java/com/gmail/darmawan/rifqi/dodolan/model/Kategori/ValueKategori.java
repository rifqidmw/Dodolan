package com.gmail.darmawan.rifqi.dodolan.model.Kategori;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yolo on 29/06/18.
 */

public class ResultKategori {
    @SerializedName("kategori")
    String kategori;
    @SerializedName("foto")
    String foto;

    public ResultKategori(String kategori, String foto) {
        this.kategori = kategori;
        this.foto = foto;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }
}

package com.gmail.darmawan.rifqi.dodolan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gmail.darmawan.rifqi.dodolan.api.BaseApiService;
import com.gmail.darmawan.rifqi.dodolan.api.UrlApi;
import com.gmail.darmawan.rifqi.dodolan.model.User.ResponseLogin;
import com.gmail.darmawan.rifqi.dodolan.model.User.ResultLogin;
import com.gmail.darmawan.rifqi.dodolan.model.User.ResultRegister;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    TextInputLayout lay_email, lay_password;
    EditText et_email, et_password;
    Button btn_login, btn_signup;
    ProgressDialog progressDialog;
    SharedPreferences preferences;

    public static final String KEYPREF = "Key Preferences";
    public static final String KEYUSERNAME = "Key Username";
    public static final String KEYPASSWORD = "Key Password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        lay_email = (TextInputLayout) findViewById(R.id.lay_email1);
        lay_password = (TextInputLayout) findViewById(R.id.lay_password1);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_signup = (Button) findViewById(R.id.btn_signup);

        preferences = getSharedPreferences(KEYPREF, Context.MODE_PRIVATE);

        if (preferences.contains(KEYUSERNAME) && (preferences.contains(KEYPASSWORD))) {
            et_email.setText(preferences.getString(KEYUSERNAME, ""));
            et_password.setText(preferences.getString(KEYPASSWORD, ""));

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            finish();
            startActivity(intent);
        }

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("Loading Login ...");
                progressDialog.show();

                final String email = et_email.getText().toString();
                final String password = et_password.getText().toString();

                if (et_email.getText().toString().equals("") && et_password.getText().toString().equals("")){
                    progressDialog.dismiss();
                    et_email.setError("Email harus diisi !!!");
                    et_password.setError("Password harus diisi !!!");
                }
                else if (et_email.getText().toString().equals("")){
                    progressDialog.dismiss();
                    et_email.setError("Email harus diisi !!!");
                }
                else if (et_password.getText().toString().equals("")){
                    progressDialog.dismiss();
                    et_password.setError("Password harus diisi !!!");
                }
                else {
                    final ResultLogin resultLogin = new ResultLogin();
                    resultLogin.setUsername(email);
                    resultLogin.setPassword(password);

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(UrlApi.URL_DODOLAN)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    BaseApiService service = retrofit.create(BaseApiService.class);
                    Call<ResponseLogin> call = service.login(resultLogin);

                    call.enqueue(new Callback<ResponseLogin>() {
                        @Override
                        public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                            if (response.isSuccessful()){
                                progressDialog.dismiss();
                                if (response.body().getStatus().equals("200")){
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString(KEYUSERNAME, email);
                                    editor.putString(KEYPASSWORD, password);
                                    editor.apply();

                                    String message = response.body().getMessage();

                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                                    startActivity(intent);
                                } else {
                                    String message = response.body().getMessage();
                                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            } else{
                                progressDialog.dismiss();
                                Toast.makeText(LoginActivity.this, "Gagal terhubung ke server !!!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseLogin> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, "Tidak dapat terhubung ke internet !!!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });
    }
}

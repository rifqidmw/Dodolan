package com.gmail.darmawan.rifqi.dodolan.model.Kategori.ShowBarang;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yolo on 02/07/18.
 */

public class ResponseBarangKategori {
    @SerializedName("id_barang")
    String id_barang;
    @SerializedName("nama_barang")
    String nama_barang;
    @SerializedName("stok")
    String stok;
    @SerializedName("kategori")
    String kategori;
    @SerializedName("ket_barang")
    String ket_barang;
    @SerializedName("harga")
    String harga;
    @SerializedName("foto")
    String foto;

    public String getId_barang() {
        return id_barang;
    }

    public void setId_barang(String id_barang) {
        this.id_barang = id_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getKet_barang() {
        return ket_barang;
    }

    public void setKet_barang(String ket_barang) {
        this.ket_barang = ket_barang;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}

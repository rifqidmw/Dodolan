package com.gmail.darmawan.rifqi.dodolan.model.User;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yolo on 28/06/18.
 */

public class ResultRegister {
    @SerializedName("nama")
    String nama;
    @SerializedName("username")
    String username;
    @SerializedName("password")
    String password;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

package com.gmail.darmawan.rifqi.dodolan.model.User;

/**
 * Created by yolo on 28/06/18.
 */

public class ResponseLogin {
    String message, user, status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.gmail.darmawan.rifqi.dodolan.model.Ongkir.Kota;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yolo on 16/06/18.
 */

public class Query {
    @SerializedName("province")
    @Expose
    private String province;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}

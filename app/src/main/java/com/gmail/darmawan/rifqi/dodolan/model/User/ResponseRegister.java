package com.gmail.darmawan.rifqi.dodolan.model.User;

/**
 * Created by yolo on 28/06/18.
 */

public class ResponseRegister {
    String message, status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

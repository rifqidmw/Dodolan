package com.gmail.darmawan.rifqi.dodolan.model.Ongkir.Provinsi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yolo on 16/06/18.
 */

public class ItemProvinsi {
    @SerializedName("rajaongkir")
    @Expose
    private Rajaongkir rajaongkir;

    public ItemProvinsi(Rajaongkir rajaongkir) {
        this.rajaongkir = rajaongkir;
    }

    public Rajaongkir getRajaongkir() {
        return rajaongkir;
    }

    public void setRajaongkir(Rajaongkir rajaongkir) {
        this.rajaongkir = rajaongkir;
    }
}

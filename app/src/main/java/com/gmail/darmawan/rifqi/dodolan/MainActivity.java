package com.gmail.darmawan.rifqi.dodolan;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gmail.darmawan.rifqi.dodolan.ImageSlider.SliderFragment;
import com.gmail.darmawan.rifqi.dodolan.ImageSlider.SliderIndicator;
import com.gmail.darmawan.rifqi.dodolan.ImageSlider.SliderPagerAdapter;
import com.gmail.darmawan.rifqi.dodolan.ImageSlider.SliderView;
import com.gmail.darmawan.rifqi.dodolan.adapter.MainBarangAdapter;
import com.gmail.darmawan.rifqi.dodolan.api.BaseApiService;
import com.gmail.darmawan.rifqi.dodolan.api.UrlApi;
import com.gmail.darmawan.rifqi.dodolan.model.Barang.ResultBarang;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private GridLayoutManager lLayout;
    private ProgressDialog progressDialog;
    private List<ResultBarang> listBarang = null;
    private RecyclerView rView;
    private MainBarangAdapter adapter;

    private SliderPagerAdapter sliderAdapter;
    private SliderIndicator sliderIndicator;
    private SliderView sliderView;
    private LinearLayout mLinearLayout;
    SharedPreferences preferences;

    public static final String KEYPREF = "Key Preferences";
    public static final String KEYUSERNAME = "Key Username";
    public static final String KEYPASSWORD = "Key Password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sliderView = (SliderView) findViewById(R.id.sliderView);
        mLinearLayout = (LinearLayout) findViewById(R.id.pagesContainer);
        setupSlider();

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        preferences = getSharedPreferences(KEYPREF, Context.MODE_PRIVATE);

        loadBarang();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setupSlider(){
        sliderView.setDurationScroll(800);
        List<android.support.v4.app.Fragment> fragments = new ArrayList<>();
        fragments.add(SliderFragment.newInstance("https://image.tmdb.org/t/p/w250_and_h281_bestv2/zYFQM9G5j9cRsMNMuZAX64nmUMf.jpg"));
        fragments.add(SliderFragment.newInstance("https://image.tmdb.org/t/p/w250_and_h281_bestv2/rXBB8F6XpHAwci2dihBCcixIHrK.jpg"));
        fragments.add(SliderFragment.newInstance("https://image.tmdb.org/t/p/w250_and_h141_bestv2/biN2sqExViEh8IYSJrXlNKjpjxx.jpg"));
        fragments.add(SliderFragment.newInstance("https://image.tmdb.org/t/p/w250_and_h141_bestv2/o9OKe3M06QMLOzTl3l6GStYtnE9.jpg"));

        sliderAdapter = new SliderPagerAdapter(getSupportFragmentManager(), fragments);
        sliderView.setAdapter(sliderAdapter);
        sliderIndicator = new SliderIndicator(this, mLinearLayout, sliderView, R.drawable.indicator_circle);
        sliderIndicator.setPageCount(fragments.size());
        sliderIndicator.show();
    }

    private void loadBarang(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.URL_DODOLAN)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        BaseApiService service = retrofit.create(BaseApiService.class);
        Call<List<ResultBarang>> call = service.getBarang();

        call.enqueue(new Callback<List<ResultBarang>>() {
            @Override
            public void onResponse(Call<List<ResultBarang>> call, Response<List<ResultBarang>> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()){
                    listBarang = response.body();

                    lLayout = new GridLayoutManager(MainActivity.this, 2);
                    rView = (RecyclerView)findViewById(R.id.recycler_view);
                    rView.setHasFixedSize(true);
                    rView.setLayoutManager(lLayout);

                    adapter = new MainBarangAdapter(MainActivity.this, listBarang);
                    rView.setAdapter(adapter);
                }
                else {
                    Toast.makeText(MainActivity.this, "Gagal mendapatkan data dari server !!!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<ResultBarang>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(KEYUSERNAME, null);
            editor.putString(KEYPASSWORD, null);
            editor.apply();

            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
